using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    public Transform charTransform;

    private Vector3 charCameraVector3;
    // Start is called before the first frame update
    void Start()
    {
        charCameraVector3 = charTransform.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = charTransform.position - charCameraVector3;
    }
}
