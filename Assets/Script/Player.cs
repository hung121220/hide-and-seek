using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //public Rigidbody charRigidbody;
    public CharacterController characterController;

    public float speed;

    public float rotateSpeed;

    public Animator animator;

    private bool isPressed = false;
    void FixedUpdate()
    {
        Animation();

        var moveZ = Input.GetAxis("Vertical");
        var moveX = Input.GetAxis("Horizontal");
        if (moveZ != 0 || moveX != 0) isPressed = true;
        else isPressed = false;
        Vector3 movement = new Vector3(moveX, 0, moveZ);
        Move(movement);
        Rotation(movement);
    }

    void Move(Vector3 movement)
    {
        //charRigidbody.MovePosition(transform.position + moveDistance * speed * Time.deltaTime);
        characterController.Move(movement * speed * Time.deltaTime);
    }

    void Rotation(Vector3 movement)
    {
        Quaternion currentRotation = transform.rotation;
        Quaternion targetRotation = Quaternion.LookRotation(movement);
        transform.rotation = Quaternion.Slerp(currentRotation, targetRotation, rotateSpeed);
    }

    void Animation()
    {
        bool isRunning = animator.GetBool("isRunning");
        if(isPressed)
        {
            animator.SetBool("isRunning", true);
        }
        else if(!isPressed)
        {
            animator.SetBool("isRunning", false);
        }
    }
}
